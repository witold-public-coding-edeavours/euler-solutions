/* Problem statement
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. 
The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
*/

#include<stdio.h>
#define UPPER_RANGE 1000
#define DIVISOR_1 3
#define DIVISOR_2 5

// Check if number is divisible
int divisibilityCheck(int number)
{
    if( ( ( number % DIVISOR_1 ) == 0 ) || ( ( number % DIVISOR_2 ) == 0 ) )
    {
        return number;
    }
    else
    {
        return 0;
    }    
}

int findSum(void)
{
    int i = 0;
    int sum = 0;
    for(i = 0; i < UPPER_RANGE; i++)
    {
        sum += divisibilityCheck(i);
    }
    return sum;
}

int main()
{
    int result = 0;
    result = findSum();
    printf("Result is: %d \n",result);
    return 0;   
}